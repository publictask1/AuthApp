package com.auth2app.auth2app.controller;


import com.auth2app.auth2app.Data.response.LinkedinAuthorizationResponse;
import com.auth2app.auth2app.service.oauth.LinkedinAuthService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;


@Controller
@RequestMapping("/api/v1/linkedin/")
@RequiredArgsConstructor
@Slf4j
public class OauthController {

    private final LinkedinAuthService linkedinAuthService;


    @GetMapping("authorize")
    public void redirectToLinkedin(HttpServletResponse response) throws IOException {
        log.info("Logging URI -------------> {}", linkedinAuthService.buildAuthorizationURI());
        response.sendRedirect(linkedinAuthService.buildAuthorizationURI());
    }


    @GetMapping("callback")
    public String linkedInCallbackURL(
            @RequestParam(name = "code") String code ,
            @RequestParam(name = "state") String state,
            HttpServletRequest request) throws IOException {

        log.info("logging code and state in controller ------> {}","state " + state + "   "+ "code "+ code);
        LinkedinAuthorizationResponse res = LinkedinAuthorizationResponse.builder()
                .code(code)
                .state(state)
                .request(request)
                .build();
        String info =  linkedinAuthService.handleCallBackRequest(res);

            return "redirect:/api/v1/linkedin/welcome?message="+info;

    }

    @GetMapping("welcome")
    @ResponseBody
    public String redirectHomepage(@RequestParam (name = "message") String message){
        return message;
    }





}
