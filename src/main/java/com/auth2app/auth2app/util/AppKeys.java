package com.auth2app.auth2app.util;


import org.springframework.beans.factory.annotation.Value;



public class AppKeys {

    @Value("${clientId}")
    public static String LINKEDIN_CLIENT_ID;

    @Value("${clientSecret}")
    public static String LINKEDIN_CLIENT_SECRET;

    @Value("${scope}")
    public static String LINKEDIN_SCOPE;

    @Value("${state}")
    public static String LINKEDIN_STATE;
}
