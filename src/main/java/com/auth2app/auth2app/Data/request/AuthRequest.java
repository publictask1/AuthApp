package com.auth2app.auth2app.Data.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthRequest {

    @NotBlank(message = "field cannot be blank")
    private String email;

    @NotBlank(message = "field cannot be blank")
    private String password;

}
