package com.auth2app.auth2app.Data.response;

import jakarta.servlet.http.HttpServletRequest;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LinkedinAuthorizationResponse {

    private String code;

    private String state;

    private HttpServletRequest request;
}
