package com.auth2app.auth2app.Data.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkedinUserInfoResponse {

       private String sub;

       private String country;

       private String given_name;

       private String family_name;

       private String email;

       private String picture;

       @Override
       public String toString() {
              return "LinkedinUserInfoResponse{" +
                      "sub='" + sub + '\'' +
                      ", country='" + country + '\'' +
                      ", given_name='" + given_name + '\'' +
                      ", family_name='" + family_name + '\'' +
                      ", email='" + email + '\'' +
                      ", picture='" + picture + '\'' +
                      '}';
       }
}
