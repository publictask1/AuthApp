package com.auth2app.auth2app.advice;

public class InvalidStateException extends RuntimeException {
    public InvalidStateException(String s) {super(s);
    }
}
