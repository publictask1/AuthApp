package com.auth2app.auth2app.service.jwtAuth;

import com.auth2app.auth2app.Data.request.AuthRequest;
import com.auth2app.auth2app.Data.request.CreateUserRequest;
import com.auth2app.auth2app.Data.response.AuthenticationResponse;


public interface UserService {

    String createUser(CreateUserRequest request);

    AuthenticationResponse authenticateUser(AuthRequest request);
}
