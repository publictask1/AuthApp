package com.auth2app.auth2app.service.oauth;


import com.auth2app.auth2app.Data.response.LinkedinAccessTokenResponse;
import com.auth2app.auth2app.Data.response.LinkedinAuthorizationResponse;
import com.auth2app.auth2app.Data.response.LinkedinUserInfoResponse;
import com.auth2app.auth2app.advice.InvalidStateException;
import com.auth2app.auth2app.models.PrincipalUser;
import com.auth2app.auth2app.repository.PrincipalUserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

@Service
@RequiredArgsConstructor
@Slf4j
public class LinkedinServiceAuthImpl implements LinkedinAuthService {


    @Value("${linkedin.oauth.clientId}")
    private String clientId;

    @Value("${linkedin.oauth.clientSecret}")
    private String clientSecret;

    @Value("${linkedin.oauth.redirectUri}")
    private String redirectUri;

    @Value("${linkedin.oauth.scopes}")
    private String scopes;

    @Value("${linkedin.oauth.state}")
    private String states;

    private final PrincipalUserRepository userRepository;


    @Override
    public String buildAuthorizationURI() {
        return  UriComponentsBuilder
                .fromHttpUrl("https://www.linkedin.com/oauth/v2/authorization")
                .queryParam("response_type","code")
                .queryParam("client_id", clientId)
                .queryParam("redirect_uri", redirectUri)
                .queryParam("state", states)
                .queryParam("scope", scopes)
                .toUriString();
    }

    @Override
    public String handleCallBackRequest(LinkedinAuthorizationResponse response) throws IOException {

        // Handle CSRF if applicable
        if (!response.getState().equals(states))
            throw new InvalidStateException("Request cannot be handled at the moment");

        String accessToken = getAccessToken(response.getCode());
        LinkedinUserInfoResponse userInfo =  getUserInfo(accessToken);

        log.info("logging access token from handleCallbackREQUEST -----> {}" , accessToken);
        log.info("logging user Info from handleCallbackREQUEST ------> {}", userInfo);

        assert userInfo != null;
        String info = registerUser(userInfo);

        if (info.equals("USER_ALREADY_EXIST")) return "USER EXIST, SIGNED IN SUCCESSFUL";

        return "AUTHENTICATED, USER SIGNED UP SUCCESSFULLY";
    }



    private String getAccessToken(String authCode) throws IOException {

        OkHttpClient httpClient = new OkHttpClient();
        LinkedinAccessTokenResponse authResponse = null;

                RequestBody formBody = new FormBody.Builder()
                .add("grant_type", "authorization_code")
                .add("code", authCode)
                .add("client_id", clientId)
                .add("client_secret", clientSecret)
                .add("redirect_uri", redirectUri)
                .build();

        Request request = new Request.Builder()
                .url("https://www.linkedin.com/oauth/v2/accessToken")
                .header("Content-Type","application/x-www-form-urlencoded")
                .post(formBody)
                .build();

        try {
            Response res = httpClient.newCall(request).execute();
            if (!res.isSuccessful())throw new IOException("INTERNAL_SERVER_ERROR");

            ResponseBody responseBody  = res.body();

            ObjectMapper mapper = new ObjectMapper();
            assert responseBody != null;

            authResponse = mapper.readValue(responseBody.string(), LinkedinAccessTokenResponse.class);
            log.info("logging mapped authResponse class ----> {}", authResponse.toString());

            return authResponse.getAccess_token();

        }catch (IOException e){
            log.info("logging error details from get userInfo ------> {}" , e.getMessage());
        }

        return "Cannot retrieve access token at the moment";
    }

    private LinkedinUserInfoResponse getUserInfo(String accessToken) {

        OkHttpClient httpClient = new OkHttpClient();
        LinkedinUserInfoResponse userInfoResponse = null;

        Request req = new Request.Builder()
                .url("https://api.linkedin.com/v2/userinfo")
                .header("Authorization","Bearer " + accessToken)
                .build();

        try {

            Response res = httpClient.newCall(req).execute();
            if (!res.isSuccessful())throw new IOException("INTERNAL_SERVER_ERROR");

            ResponseBody responseBody  = res.body();

            ObjectMapper mapper = new ObjectMapper();
            assert responseBody != null;

            userInfoResponse = mapper.readValue(responseBody.string(), LinkedinUserInfoResponse.class);
            return userInfoResponse;

        }catch (IOException e){
            log.info("logging error details from get userInfo ---> {}", e.getMessage());
        }

        return null;
    }
    private String registerUser(LinkedinUserInfoResponse userInfo) {

        if (userRepository.findByEmail(userInfo.getEmail()).isPresent()) {
            return "USER_ALREADY_EXIST";
        }

        PrincipalUser user = PrincipalUser.builder()
                .firstName(userInfo.getGiven_name())
                .lastName(userInfo.getFamily_name())
                .email(userInfo.getEmail())
                .roles("ROLE_USER")
                .build();

        PrincipalUser savedUser = userRepository.save(user);

        return String.format("user with %s has successfully signed up", savedUser);
    }
}
