package com.auth2app.auth2app.service.oauth;

import com.auth2app.auth2app.Data.response.LinkedinAuthorizationResponse;

import java.io.IOException;

public interface LinkedinAuthService {

    String buildAuthorizationURI();

    String handleCallBackRequest(LinkedinAuthorizationResponse response) throws IOException;
}
