## 📁 PROJECT DESCRIPTION
A simple Oauth2.0 application demonstrating how LinkedIn Oauth2.0
works for signing up a user and also signing in. This project also
highlight how JWT authentication and authorization works
for securing application endpoints
## 💾 INSTALLATION
To run this application follow the instructions below:
1. Clone the repository
2. Install dependencies
# ⚙️ CONFIGURATION
After Installing the dependencies, you need to create some couple of
file to make the application run successfully
1. Create application-local.properties file in your resources folder
which will contain all necessary database configuration (In this project by default)
we use the MySql Database hence you will need to configure it in your
resources file.


2. You need to create an App with LinkedIn on their developer's platform
(www.linkedin.com/developers) once an app is created kindly configure
a 3-legged authentication on profile, this will provide
you with your client_id, client_secret and scope.


3. Add a callback url to the created application "http://localhost:8080/api/v1/linkedin/callback"


4. Supply all the necessary keys to your application.properties file


4. RUN THE APP 🚀🚀🚀

# 🔌 ENDPOINTS
### LINKEDIN AUTH ENDPOINT
This api makes authentication and authorization request

http://localhost:8080/api/v1/linkedin/authorize

### JWT ENDPOINTS
For these endpoints, you can test through your postman

This endpoint create user (sign up)

http://localhost:8080/api/v1/user/signup

This endpoint log in  (sign in)

http://localhost:8080/api/v1/user/signup

#### Thanks for reading, for more info you can reach me through 👇

## ✉️ CONTACT
EMAIL : abolajiidiisu@gmail.com , 07068417681